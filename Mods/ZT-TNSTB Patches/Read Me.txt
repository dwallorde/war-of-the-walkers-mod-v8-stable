 
 Ztensity's NerdScurvy's Toybox Patches - Version 1.0.0
 Blocks XML Edit For Cleaner Creative Menu
 Build 19.3 Compatible
 
 
 ================================
 
 
 ABOUT THIS MOD
 =======
 
 I noticed that there are 236 individual toys that are literally replicas/copies of the base 30 variations of 10 unique toys. While I'm unsure of what the point of all 236 copies are, I hid them from the creative menu so that things are cleaner when navigating these menu's.
 
 
 REQUIREMENTS
 =======
 
 After installing War of the Walkers overhaul, you must keep NerdScurvy's Toybox modlet in order for this modlet to work and/or not show any errors.
 
 If you delete NerdScurvy's "Toybox" modlet, delete this modlet as well to prevent errors.
 
 
 ================================
 
 
 WHAT FILES ARE NEW
 =======
 
 I added in a blocks.xml file to insert code which cleans up the creative menu.
 
 
 ================================
 
 
 CREDITS
 =======
 
 Thank you Dwallorde for a fantastic modlet!
 
 
 Guppy's Discord: https://discord.com/invite/q7uUqSw4w8
 Dwallorde's "War of the Walkers" Discord: https://discord.gg/xBnQ6NXbSb
 Ztensity's Modlets: https://bit.ly/38pnCsq
 
 
 - Ztensity -
 
 
 ================================